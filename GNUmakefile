# TODO: generate this file
# [ ] (should do) means it's left to be incorporated in real CI
# [x] (done     ) means it had been incorporated in real CI
# [-] (won't do ) means it is specific to scaffolding or a "makefile" generator

# [x] Incorporated as Definition.shell_preference
SHELL := bash -e

# [-] scaffold specific
INCORPORATE := ,incorporate/

# [-] scaffold specific (see TMI.md#avoid-runtime-detections)
MK_DIRNAME = \
	mkdir -p "$$(dirname "$@")"

# [ ] Depending on where this is needed, it might be some kind of
#     meta-generator (generator overviewer), or specific to makefile generator.
#     EDIT: It is a meta-generator, consumed by make-generator
TARGETS := \
	help clean \
	ntr ntr-watchlist ntr-on-change \
	test-tree \
	mit render render-commit generate \
	config markdown-stuff makefile-meta

# [ ] This is definitely a kind of meta-generator.
RENDER_TARGETS := \
	_.target/lib \
	_.target/makefile-meta.json \
	.gitignore \
	.github/workflows/always.yaml

# [x] In real life this strictly equals render_targets
#     Right here it is also a make-specific trick (or perhaps
#     a local-environment specificity)
CLEAN_TARGETS := \
	${RENDER_TARGETS} \
	target \
	_.target \
	_.run-jq.pipe

# [ ] !!!!
#     A very difficult generator
#     (this generates {{!! .gitignore !!}} )
IGNORE := \
	${CLEAN_TARGETS} \
	_.ntr.on_change.local

# [ ] This is a snippet something, part of some "ntr local watcher" module.
cmd_ntr_watchlist := \
	find . \( \
		-path ./${INCORPORATE}template.github.workflows.always.jq -o \
		-path ./${INCORPORATE}test_head.jq -o \
		-path ./${INCORPORATE}literal_jq.jq -o \
		-path ./${INCORPORATE}literal_jq.rx -o \
		-path ./GNUmakefile -o \
		-path ./lib/jq/config.jq/'*.jq.md' -o \
		-path ./lib/jq/config.jq/'*.jq' -o \
		-false \
	\) -print0

# [-] This command is make-specific
# [ ] The jq-split-xargs0 trick should be a snipper or something (snipper = snippet)
cmd_ntr_watchlist_list := ${cmd_ntr_watchlist} | jq -R -r -s '(split("\u0000")[] | select(length != 0)), "_.ntr.on_change.local"'

.PHONY: ${TARGETS}
# [ ] Meta-target and then imported by make-gen
#     List available targets
help:
	@jq -n -r --arg targets '${TARGETS}' '$$targets | split(" ") | map("- " + .) | .[]'

# [-] Not needed in CI, but if needed it's reverse apply gen-patch
clean: _.target/makefile-meta.json
	rm -rf ${CLEAN_TARGETS}

# [-] ntr-module, ignore
ntr-on-change:
	clear
	${MAKE} clean
	${MAKE} generate
	${MAKE} config
	${MAKE} test-tree
	${MAKE} mit
	./_.ntr.on_change.local
# [-] ntr-module, ignore
ntr:
	@${cmd_ntr_watchlist_list} | entr sh -c '${MAKE} ntr-on-change'
# [-] ntr-module, ignore
ntr-watchlist:
	@${cmd_ntr_watchlist_list}

# [ ] DO properly -- commit management
#     This is "commit human"; non generated changes (== source changes)
mit:
	${cmd_ntr_watchlist} | xargs -0 git add
	${cmd_ntr_watchlist} | xargs -0 git commit --message '[checkpoint]' --
	git push -f gh HEAD

# [ ] Interesting
.gitignore:
	for file in ${IGNORE}; do echo $$file >>$@; done

# [ ] Output root configuration
#     run generated config.jq
config: generate
	jq -n -L./_.target/lib/jq/config.jq -f _.target/lib/jq/config.jq/config.jq
### Markdownn stuff
# .jq.md -> .md
markdown-stuff:
	find ./lib/jq/config.jq -name '*.jq.md' -print0 \
| xargs -0 jq -n -r --args '$$ARGS.positional | map(gsub(".jq.md";".jq") | @sh "${MAKE} _.target/\(.)") | join(" &&\n")' \
| sh
_.target/%.jq: %.jq.md ${INCORPORATE}literal_jq.jq
	${MK_DIRNAME}
	jq --raw-input --slurp --raw-output --rawfile rx ${INCORPORATE}literal_jq.rx -f ${INCORPORATE}literal_jq.jq <$< >$@

.github/workflows/always.yaml: markdown-stuff
	${MK_DIRNAME}
	{ jq -e -n -f ${INCORPORATE}/template.github.workflows.always.jq || exit 1; } | tee $@

test-tree:
	${MAKE} clean
	${MAKE} config
	git reset ${TARGET}
	git diff -z --numstat \
| xargs -0 jq -n --slurpfile mk _.target/makefile-meta.json -e -f ${INCORPORATE}test_head.jq --args

generate: markdown-stuff ${RENDER_TARGETS}
render: generate
	${MAKE} render-commit
	git push --force-with-lease
render-commit:
	git add -f ${RENDER_TARGETS}
	git commit --no-verify --message render -- ${RENDER_TARGETS}

makefile.meta:
_.target/makefile-meta.json:
	${MK_DIRNAME}
	{ \
	jq -n --args '{targets: $$ARGS.positional}' ${TARGETS}; \
	jq -n --args '{render_targets: $$ARGS.positional}' ${RENDER_TARGETS}; \
	jq -n --args '{clean_targets: $$ARGS.positional}' ${CLEAN_TARGETS}; \
	jq -n --args '{ignore: $$ARGS.positional}' ${IGNORE}; \
} | jq --slurp 'add' >$@
