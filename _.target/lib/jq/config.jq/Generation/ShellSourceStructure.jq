def Command_Typeinfo: {
    exec: "string",
    cwd: "string",
    args: ["string"],
    env: ["string"]
};

def Command_Default: {
    exec: null,
    cwd: ".",
    args: [],
    env: []
}

def Typeinfo: {
    type: "array",
    args: [Command]
};

