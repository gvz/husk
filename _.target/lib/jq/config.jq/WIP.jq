import "./Definition.Module" as Module;

def data: {}
| .module = Module::data
;

def Typeinfo: {

def StructuredFile_typeinfo: {
    name: { type: "enum", args: ["md"] },
    payload: { type: "object" }
};

    | (
        if (.type | type) != "string"
        then error("StructuredFile.type is not a string")
        else . end
    )

    $payload

