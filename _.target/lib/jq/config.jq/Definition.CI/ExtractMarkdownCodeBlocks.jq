def markdown_code_block_regular_expression: "\n##\n# Regular expression for matching markdown code blocks.\n#\n# Runs against the complete file and is extended.\n\n##\n# Match the starting sequence\n\n    `{3}\n\n##\n# followed by any number of space or tabs.\n\n    [ \\t]*\n\n##\n# Capture by name any following word characters, optionally.\n#\n# This we will re-export as `entry`, which names the intend of the code in the block.\n\n    (?<entry>\\w*)?\n\n##\n# Optionally capture the rest of the line as `args`\n\n    (?<args>[^\\n]*) \\n\n\n##\n# The remaining text, up until before the closing sequence, is that block `content`.\n\n    (?<content>\n\n        # Match anything that is not three consecutive `s\n        (\n            `{0,2}[^`]\n        )*\n    )\n\n##\n# Match three consecutive `s -- the ending sequence\n\n    `{3}\n";
def markdown_code_blocks_regular_expression_flags: "xg\n";
# Extract markdown code blocks.
#
# Probably good idea to check [JQ PCRE] at this point.
#
def markdown_code_blocks:.

|   match(
        markdown_code_blocks_regular_expression;
        markdown_code_blocks_regular_expression_flags
    )

|   .captures

|   map(.

    |   select(.name)

    |   {key: .name, value: .string}

    )
|   from_entries

;

