# vim: et ts=4 sw=4

match($rx; "gx")
|   .captures
|   map(.
    |   select(.name)
    |   {key: .name, value: .string}
    )
|   from_entries
|   (
        empty
        , (
            .
            |   select(.entry == "jq" and .args == "")
            |   .content
        )
        , (
            .
            |   select(.entry == "jqstring")
            |   (.args | gsub("^\\s+"; "")) as $name
            |   "def \($name): \(.content | @json);"
        )
    )
