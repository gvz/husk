(.
# Output items of git diff -z
| $ARGS.positional

# Each item tab-separated
| map(split("\t"))

# Keep only path (the rest are lines added/removed)
| map(. as [$_, $_, $path] | $path)

# Test whether path begins with any "render target"
| map(startswith($mk[0].render_targets[]))

# Any "true" is evail
| if any then error("Changes in generated content") else true end
)
