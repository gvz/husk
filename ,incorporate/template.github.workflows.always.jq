# vim: et ts=4 sw=4
{
on: "push",

jobs: {
    "test-worktree": {
        "runs-on": "ubuntu-20.04",
        steps: [{
            name: "prop::no_gen_chasm",
            run: (
                "git config --global user.name ParaCI; \\\n" +
                "git config --global user.email ParaCI@paraci.infra.blue; \\\n" +
                "git clone https://github.com/Good-Vibez/husk && " +
                "cd husk && \\\n" +
                "make test-tree TARGET=HEAD \\\n"
            )
        }]
    }
}
}
