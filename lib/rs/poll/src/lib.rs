//! # `Poll = Result<Option>`
//!
//! [`Poll`] comes in handy as a building block for [MBI].
//!
//! ## Move based iteration
//!
//! Normally iteration looks like `<T> FnMut() -> Option<T>` or `(&mut self) -> Option<T>`.
//! However, there are occasions when the complete restructure of self is needed.
//!
//! Move-based (or state-based) iteration looks a bit more like this:
//!
//!     type Poll<T, E> = Result<Option<T>, E>;
//!     type NextF<S, E> = fn(S) -> Poll<S, E>;
//!
//! In terms of compiler demands, this is only more flexible when moving objects between
//! different instances of enum. Consider the following:
//!
//! ```rust
//! # #[macro_use] extern crate poll;
//! use {std::io, poll::{self, Poll}};
//!
//! /// The device can be "on", holding a source and a buffer,
//! /// or "off", holding just the last state (buffer).
//! pub enum Device {
//!     On { source: Box<dyn io::Read>, state: String },
//!     Off { state: String },
//! }
//!
//! let source = "Hello".bytes().collect::<Vec<_>>();
//! let source = Box::new(io::Cursor::new(source));
//!
//! let d0 = Device::On { source, state: "".to_owned() };
//! let d1 = Device::Off { state: "".to_owned() };
//!
//! let together = (d0, d1);
//!
//! // In this example it is impossible to create an `FnMut() -> Option<T>` function that can
//! // take pair `(d0, d1)`, and, in-memory, move `source` from `d0` to `d1`.
//!
//! // If we try this, the compiler will cry about moving things behind a reference.
//!
//! // However, it is perfectly natural and possible to have this instead:
//!
//! type State = (Device, Device);
//! let next: fn(State) -> Poll<State, ()> = |state| match state {
//!     (Device::On { state: a, source }, Device::Off { state: b }) => {
//!
//!         // Move things around
//!
//!         let a = Device::Off { state: a };
//!         let b = Device::On { state: b, source };    // <- wow
//!
//!         let next_state = (a, b);
//!         poll::ok(next_state)
//!     }
//!     _ => panic!("invalid")
//! };
//!
//! match next(together) {
//!     Ok(Some((Device::Off { .. }, Device:: On { .. }))) => {
//!         // ok
//!     }
//!     _ => assert!(false)
//! }
//! ```
//!
//! [MBI]: #move-based-iteration
//! [`Poll`]: type.Poll.html
//! <!-- TODO: make into a crodule modurate cradule -->
pub type Poll<T, E> = Result<Option<T>, E>;

/// Signal end-of-iteration: `Ok(None)`
///
/// # Examples
///
/// ```rust
/// use poll::Poll;
///
/// fn f() -> Poll<String, ()> {
///     poll::done()
/// }
///
/// assert_eq!(f(), Ok(None));
/// ```
pub fn done<T, E>() -> Poll<T, E> {
    Ok(None)
}
#[test]
fn done_() {
    assert_eq!(done::<Vec<String>, Vec<u8>>(), Ok(None));
}

/// Yield successfully the next state: `Ok(Some(T))`
///
/// # Examples
///
/// ```rust
/// use poll::Poll;
///
/// fn f() -> Poll<u8, String> {
///     poll::ok(4)
/// }
///
/// assert_eq!(f(), Ok(Some(4_u8)));
/// ```
pub fn ok<T, E>(obj: T) -> Poll<T, E> {
    Ok(Some(obj))
}
#[test]
fn ok_() {
    assert_eq!(ok::<_, ()>(8_u8), Ok(Some(8)));
}

/// Yield an error: `Err(err)`
///
/// # Examples
///
/// ```rust
/// use poll::Poll;
///
/// fn f() -> Poll<(), u32> {
///     poll::error(12_u32)
/// }
///
/// assert_eq!(f(), Err(12));
/// ```
///
/// It can also handle `Into` flows.
///
/// ```rust
/// use poll::Poll;
/// #[derive(Debug, PartialEq)] struct ErrorTarget;
/// #[derive(Debug, PartialEq)] struct ErrorSource;
///
/// impl From<ErrorSource> for ErrorTarget {
///     fn from(err: ErrorSource) -> Self { Self }
/// }
///
/// fn f() -> Poll<String, ErrorTarget> {
///     fn internal_open() -> Poll<String, ErrorSource> {
///         poll::error(ErrorSource)
///     }
///
///     // In real life use `poll!()` macro.
///     match internal_open() {
///         Ok(Some(io)) => poll::ok(io),
///         Ok(None) => poll::done(),
///         Err(err) => {
///             // This is still a source error.
///             let err: ErrorSource = err;
///
///             // But here we need to return a
///             // target error.
///             poll::error(err)
///         }
///     }
/// }
///
/// assert_eq!(f(), Err(ErrorTarget));
/// ```
pub fn error<T, E, U: Into<E>>(e: U) -> Poll<T, E> {
    Err(e.into())
}
#[test]
fn error_() {
    assert_eq!(error::<(), u32, _>(6_u8), Err(6_u32));
}

/// Unwrap the inner value, or return the respective `None` or `Err`.
///
/// # Exaples
///
/// ```rust
/// # #[macro_use] extern crate poll;
/// use poll::Poll;
///
/// fn returns_ok() -> Poll<u8, ()> {
///     let value = poll::ok(8);
///     let value = poll!(value);
///     poll::ok(value)
/// }
/// assert_eq!(returns_ok(), Ok(Some(8)));
///
/// fn returns_done() -> Poll<u8, ()> {
///     let value = poll::done::<u8, ()>();
///     let value = poll!(value);
///
///     // We'll never get here
///     panic!("omg!");
/// }
/// assert_eq!(returns_done(), Ok(None));
///
/// fn returns_err() -> Poll<(), u8> {
///     let value = poll::error::<(), u8, _>(12);
///     let value = poll!(value);
///
///     // We'll never get here
///     panic!("omg!");
/// }
/// assert_eq!(returns_err(), Err(12));
/// ```
#[macro_export]
macro_rules! poll {
    ($e:expr) => {
        $crate::poll_opt!($e?)
    };
}

#[macro_export]
macro_rules! poll_opt {
    ($e:expr) => {{
        let value;

        if let Some(v) = $e {
            value = v;
        } else {
            return $crate::done();
        }

        value
    }};
}
