# `Definition.Sourceballs`

Here we define types of sourceballs.

## Sourceballs?

Sourceballs are no more than a collection of source files (as file paths).
We put them in a "ball" because they somehow relate all together, in forming a project.

Sourceballs don't necessarily contain only one language code. Could involve pretty much
everything you'd add to git.

We use them just as lists of files that we need to process in [`Generation`].

## Sourceballs
```jq
def Sourceballs: {
```

### `.ConfigJq`

```jq
    .ConfigJq: {
    }
```

> all markdown files of this executable specification


```jq
}   # .Sourceballs ends
```

[`Generation`]: Generation.jq.md

<!-- vim: et ts=4 sw=4
-->
