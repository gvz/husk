# Definition

The root of evil.

## Define everything

Definition defines, by definition, everything.

Generally, anything of interest.

| Stuffies | Why? | What? |
|---|---|---|
| [`.CI`] | Commom source for CI things | generic CI description |
| [`.Sourceballs`] | Source files of interest packed together | a list of filenames |

[`.CI`]: ./Definition.CI.jq.md
[`.Sourceballs`]: ./Definition.Sourceballs.jq.md

<!-- vim: et ts=4 sw=4
-->
