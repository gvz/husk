# `Definition.CI`

`@TODO link: project name` is able to generate its very own CI.

Here are stuff that will help us build a consistent CI culture.


| stuffies | why? | what? |
|---|---|---|
| [`.shellpreference`] | one way to execute arbitrary commands | command prefix |
| [`.extractmarkdowncodeblocks`] | very handy to bootstrap | `regexp`/`jq` black magic |

[`.ShellPreference`]: Definition.CI/ShellPreference.jq.md
[`.ExtractMarkdownCodeBlocks`]: Definition.CI/ExtractMarkdownCodeBlocks.jq.md

<!-- vim: et ts=4 sw=4
-->
