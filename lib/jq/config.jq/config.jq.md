<!-- vim: et ts=4 sw=4
-->
# config.jq

`config.jq` is a tool for generating configurations.

It works by pure data transformations.

```todo
- [ ] mention and link JQ
```

---
## Definition.jq

Definition is all input data. For clarity we write it in a different file:
[`Definition`]. In that file you can see what definitions are contained
and how to add more.

```jq
import "Definition" as Definition;
```

Definitions live in a free-for-all namespace. [`Definition.json`] is simply a
root JSON object, holding all definitions under one.

To inspect the final output: `./config.jq | jq .Definition`.

```todo
- [ ] link or include the above
- [ ] add reference to Definition.json
```

*Related:*

- [`Definition`]
- [`Definition::data`]
- [`Definition.json`]
- [`Definition.jq`]

---
## Generation

Similar to `Definition`, [`Generation`] is the root object of all generation
filters.

There are many subproblems hiding under this problems. To look deeper, look into [`Generation`].

*Related:*

- [`Generation`]
- [`Generation::data`]
- [`Generation.json`]
- [`Generation.jq`]


```jq
import "Generation" as Generation;
```

---
## Web of Lies

Links to everywhere.

* [TMI]: Stuff you don't want to read about
* [`WIP`]: Broken code and irregular text waiting to be woven into main

---
## Summary / hypercondensation

    Definition         => getFiles  ->
    FileList           => load      ->
    RawFiles           => parse     ->
    StructuredFiles    => generate  ->
    StructuredFiles    => render    ->
    RawFiles           => send      ->
    Patch(es)


---
## Next Targets

```todo
- [ ] Module definition system
- [ ] Module definitions
- [ ] Load module meta
- [ ] List module files
- [ ] Generate "Requested Links" from .jq.md source (for module)
- [ ] Lint: Check broken links
- [ ] Tables of Contentses
- [ ] Easy access to diff per code class (genereated, human, stats, meta, who knows)
- [x] scaffold:make: build .jq.md to .jq
- [x] scaffold:make: test config.jq
- [x] scaffold:make: generate - commit - push
- [x] scaffold:make: lint commit (contains changes to generated content)
- [-] scaffols:make: lint commit (contains newer config) [AKA `prop::no_config_chasm`]
- [ ] [advanced] scaffols:make: lint commit (contains older config) [AKA `prop::no_config_chasm` ++]
- [ ] [crazy] generate reverse git history ;;lends well for reviewing auto-commit changes in GH/GL history-clicking
- [ ] gen: jq defs/imports => md headers, for linking scheme
- [ ] gen: intermediate outputs per jq-def (wow)
```

---
## Module Structure

We follow a module/plugin structure for adding components. See [`Module`].

```todo
- [ ] broken links?
```

[`Definition`]: Definition.jq.md
[`Definition::data`]: Definition.jq.md#L6
[`Definition.json`]: _.target/json/Definition.json
[`Definition.jq`]: _.target/Definition.jq
[`Generation`]: Generation.jq.md
[`Generation::data`]: Generation.jq.md#L6
[`Generation.json`]: _.target/json/Generation.json
[`Generation.jq`]: _.target/Generation.jq
[`Module`]: Module.jq.md
[JQ Filters]: https://stedolan.github.io/jq/manual/#Basicfilters
[TMI]: TMI.md
[`WIP`]: WIP.jq.md
