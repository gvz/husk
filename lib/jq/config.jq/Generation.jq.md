<!-- vim: et ts=4 sw=4
-->
# Generation

## `#funpart`

This is fun part.

## The grand pipeline

### Non-self-referential-static-single-source

At this point we can imagine that we have everything that can be
defined, constantly, permanently (semi), out of nothing. You might
see the code-name `eternal` for such types of things somewhere
mentioned.

This [Definition] is completely static and absolutely non-self-referential.
This is absolutely guaranteed by the way [Definition] is constructred.

Files loaded by [Definition.jq] *do not* have access to other definitions
from other files. This means that every definition module is independent and
isolated from the rest. It also means it can consistently produce the
same `eternal` definition no matter what. Don't believe us? See for yourselves:
[Definition].

> all I had to do was type `[Definition]`, btw

> [EDIT] and then build an MD documentation system in jq....

In rust terms we consider `eternal` stuff as
```rust
type Eternal<T> = &'static T;
```
but this is irrelevant.

### Stages and [`metaphor::weir`]

We will **not** go with "stages", because it is boring and already suffering
from overload and un-wanted side-connotations, from being hogged by GitlabCI.

Introducing [`metaphor::weir`]. They are the same, but on the pure declarative
level of [`app::config.jq`].

What makes them same is the core principle, which is the only thing that survives
here:

> Earlier stages complete before later ones.
>
>   ~ Gitlab CI

In this case this adapts to

> Earlier weirs flow into later ones.
>
>   - Dev Op Buddha ~DOB

The wording is perfect, on one hand because it reflects sharply the fact that there
is nothing:

- to happen
- to complete
- to wait
- to fail

but most importantly because it states *directly* that generation works in *layers*
(through which *one huge signle instance* of the `Generation` root JSON object is
"flowing").

### Generation Ancestry

Not all generations are the same.

Generations can require data from previous generations. This is the weir(d) structure.

Generations are placed in as early **Place** as possible. 

@TODO insert algorithm, because it's easier than explaining
```algorithm
    foreach Generation
        if it can be Place[0], it is
        if it can be Place[1], it is
        ...
```

#### When can a Generation **not** be in a place?

```algorithm

A **Generation.A** is **banned** from **Place[N]** if

    there exists Generation.B
    such as     
                Generation.A depends_on Generation.B
    and         Generation.B.place <= N
```

*Note*: Obviously the `B.place` is a join-query and is the hidden risk of this pseydocode.


### Weir structure

In [Definition.jq] we aim for `eternal` semantics and definition isolation, therefore we use
the `module::data` approach. The top-level [`Definition.jq`] takes care of composing those into
a root object, and thus statically guarantees name uniqueness.

In Generation, though, we are restricted by the fact that we want to avoid having to
define our generators declaratively in jq.

> #### Clarification on declarative generators
> 
> This would mean that we describe generators are jq primitives in JSON data.
> For example:
> 
> ```jq#example
>     {
>         source: ["Definition", "Module", {type: "number"}, "name"],
>         destination: ["Generation", "readmez", "modules", {array: "append"}]
>     }
> ```
> 
> Note how impossible it gets when trying to match wildcards, loop, refere, or anything special.

In order to avoid the above, Generations are defined directly as JQ modules. Instead of
communicating through a Data Interface, Generations instead communicate with the pipeline
in the form of **plug-in**s, by respecting a specific JQ "API", in the module scope.

**Exception** to this is the fact the meta-information, specific to generators, could flow down
to later generators (which is a forma of declarative generators). Prominent example of this
is the CI generations, which need a "recursive-make" like command, that allows them to invoke
other CI targets at runtime.


## Data types and interfaces

`@todo [after head-stabilize] sync/include/gen headers to happenings`

### StructuredFile
```jq
import "Generation/StructuredFile" as StructuredFile;
```
A structured file indicates that it is a source file in parsed form.

It helps only document the interface and provide runtime checks.


### FileLoaders

File loaders can parse a source input string to a structured File.

```jq
```

[`metaphor::weir`]: https://duckduckgo.com/?q=weir&ia=images&iax=images
