# ShellSourceStructure

`ShellSourceStructure` is a structured file format that expresses
simply pipelines of shell commands.

It is much like coding pipelines in shell, except way more
comfortable in regards to escapes, exit codes, syntax, et c.

## Command-Typeinfo
```jq
def Command_Typeinfo: {
    exec: "string",
    cwd: "string",
    args: ["string"],
    env: ["string"]
};
```

## Command-Default
```jq
def Command_Default: {
    exec: null,
    cwd: ".",
    args: [],
    env: []
}
```

## Typeinfo
```jq
def Typeinfo: {
    type: "array",
    args: [Command]
};
```

<!-- vim: et ts=4 sw=4
-->
