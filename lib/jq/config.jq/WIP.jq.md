## Modulation.jq

[`Modulation`] is the root of all module import definitions.

Module imports are like declaring modules to be managed by
[`config.jq`].

`config.jq` does not impose any specific contracts, restrictions or
guarantees to what a module import definitions are. This data is passed
through to the rest of the pipeline and is mainly of interest to
[Generation].

[`Modulation`] is a JSON object, and the only thing naturally enforced is
the uniqueness of name per entry.

```jq
import "./Definition.Module" as Module;

def data: {}
| .module = Module::data
;
```

---
---
---

#### StructuredFile-Typeinfo

```jq
def Typeinfo: {
```

The structured file has two fields:

Here is the type info:

```jq
def StructuredFile_typeinfo: {
    name: { type: "enum", args: ["md"] },
    payload: { type: "object" }
};
```



#### StructuredFile.type
```jq
    | (
        if (.type | type) != "string"
        then error("StructuredFile.type is not a string")
        else . end
    )
```

`type` names the file type.

```sh
#!example @todo test/sync/include

./config.jq | jq .Generation.StructuredFile.type.values
# => ["rs", "json", "tf", "md", ...]
```

#### StructuredFile.payload
```jq
    $payload
```

`payload` contains arbitrary data, specific to the [StructuredFile.type].
