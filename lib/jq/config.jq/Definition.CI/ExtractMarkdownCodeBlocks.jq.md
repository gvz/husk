# `ExtractMarkdownCodeBlocks`

It comes in handy to be able to extract code blocks (surrounded in \`)
from markdown, or other document formats.

In this section we define stuff relative to that.

The approach for this goal is minimal. We load the complete file contents
in memory, and extract code blocks with a regular expression. We then turn
the regular expression match to a meaningful JSON object, and use that in
later generators.

## Regular expression

Lets break this down.

> This analysis can also be found at https://regex101.com/r/bwbKeo/2 .

This will be a **g**lobal, e**x**tended regular expression. This means it
will match against the complete file, and we can use spaces and comments in
the regular expression itself.

```jqstring markdown_code_block_regular_expression

##
# Regular expression for matching markdown code blocks.
#
# Runs against the complete file and is extended.

##
# Match the starting sequence

    `{3}

##
# followed by any number of space or tabs.

    [ \t]*

##
# Capture by name any following word characters, optionally.
#
# This we will re-export as `entry`, which names the intend of the code in the block.

    (?<entry>\w*)?

##
# Optionally capture the rest of the line as `args`

    (?<args>[^\n]*) \n

##
# The remaining text, up until before the closing sequence, is that block `content`.

    (?<content>

        # Match anything that is not three consecutive `s
        (
            `{0,2}[^`]
        )*
    )

##
# Match three consecutive `s -- the ending sequence

    `{3}
```

We need to accompany the RegExp with its flags. As mentioned earlier, we want it
to be e`x`tented for the comments, and `g`lobal so it can match all code blocks in
the string.

```jqstring markdown_code_blocks_regular_expression_flags
xg
```

## JSON output

We then turn matches into JSON objects.

```jq
# Extract markdown code blocks.
#
# Probably good idea to check [JQ PCRE] at this point.
#
def markdown_code_blocks:.
```

1. Match input string against the regexp
```jq
|   match(
        markdown_code_blocks_regular_expression;
        markdown_code_blocks_regular_expression_flags
    )
```

2. take the captures
```jq
|   .captures
```

2. for each capture
```jq
|   map(.
```

2. filter by existence of `name` (so that un-named captures are skipped)
```jq
    |   select(.name)
```

3. make a key-value object-entry-pair from capture's name and value
```jq
    |   {key: .name, value: .string}
```

4. make an object from those key-value entries
```jq
    )
|   from_entries
```

This is it.
```jq
;
```

## Example

```todo
- [ ] test
```

Piping this markdown

```markdown

# Title one

```hawaiscript  non separated arguments, including leading and trailing space

while true {
    write(codez);
}
```

through `markdown_code_blocks` results in this JSON

```json

```



[`extern::shebang`]: https://en.wikipedia.org/wiki/Shebang_(Unix)
[`Action`]: ./Action.jq.md
[#given]: #given
[JQ PCRE]: https://stedolan.github.io/jq/manual/#RegularexpressionsPCRE

<!-- vim: et ts=4 sw=4
-->
