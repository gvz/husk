# `ShellPreference`

We define the preference of a shell, for [`Action`]s that care to run inside
one, or to invoke commands with one.

This preference **`@should`** be included by every generetor that generates for
running (inside) a shell.

This acts pretty much like a [`extern::shebang`] in most places it is used.
Which means that generators, who use this preference, **`@must`** respect the
fact that shell preference expresses a *command prefix* (and not a single command).

### Example

```todo
- [ ] Turn to spec
```

Given (1) a shell preference, like

```jq
def shell_preference:
    ["/usr/bin/env", "-S", "bash", "-e", "-x"]
;
```

and (2) a custom `bash` executable,
(3) found under `$PATH`,
(4) in `/uzr/locco/bean`,
makes commands run in a `/uzr/locco/bean/bash -e -x` environment.


### Non coincidentally

The shell preference in the [#given] above, is the also the one we use.
